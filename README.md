# singularity services (instance)

### Use singularity image as services (daemon)

https://sylabs.io/guides/3.9/user-guide/running_services.html?highlight=instance

#### Apache2 web server

sudo singularity instance list
sudo singularity instance start apache.min.sif apache2
sudo singularity instance stop apache2

### Could be use in 3 possibilities


## 1) regular sif image and bind rw folders

- Build image
```bash
sudo singularity build service_apache.sif singularity.apache.min.def
```

- create folder that will be inn rw mode
```bash
mkdir -p apacherw/run/apache2
mkdir -p apacherw/run/lock/apache2
mkdir -p apacherw/var/log/apache2
```
- Start the image instance
```bash
sudo singularity instance start --bind apacherw/run/apache2:/run/apache2,apacherw/run/lock/apache2:/run/lock/apache2,apacherw/var/log/apache2:/var/log/apache2 apache.min.sif apache2
```

#### !!! We have to anticipate all the RW files/folders


## 2) use permanant overlay
https://sylabs.io/guides/3.9/user-guide/persistent_overlays.html

- Build image
```bash
sudo singularity build service_apache.sif singularity.apache.min.def
```
- Create the permanant overlay folder
```bash
mkdir -p overlay_apache2
```

- Start the image instance
```bash
sudo singularity instance start --overlay overlay_apache2/ apache.min.sif apache2
```

## 2) build the image in a sandbox


- Build image
```bash
sudo singularity build --sandbox service_apache.sandbox singularity.apache.min.def
```

- Start the image instance
```bash
sudo singularity instance start --writable apache.min.sif apache2
```


## Access to the web page:
```bash
http://myserver:8181/
```


